# BootcampBRI-MiniProject1

Nama : Pascal Pribadi Akhmad Panatagama \
Kelas Backend 1

Soal : https://docs.google.com/document/d/1W_4B7VTboh8Nhk2CRePcBalTV5gBlVD8/edit

- Buat user \
CREATE USER 'superadmin'@'0.0.0.0' IDENTIFIED BY 'superadmin';

- Grant Priviledges \
GRANT ALL PRIVILEGES ON * . * TO 'superadmin'@'0.0.0.0';

- mysqldump (export sql) \
mysqldump -u <username> -p crm_service > crm_service.sql

## ERD

![erd](https://gitlab.com/pascalpanatagama/bootcampbri-miniproject1/-/raw/main/MiniProject1-Page-2.drawio.png)
